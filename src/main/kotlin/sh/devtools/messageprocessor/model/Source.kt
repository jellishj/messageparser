package sh.devtools.messageprocessor.model

data class Source(
    val name: String,
    val pictureSource: String
)