package sh.devtools.messageprocessor.model

import java.time.Instant

data class Message(
    val timestamp: Instant,
    val source: Source,
    val contents: String
)

fun messageFromCsv(array: Array<String>): Message {
    return Message(
        timestamp = Instant.ofEpochSecond(array[0].toLong()),
        source = Source(
            name = array[1],
            pictureSource = ""
        ),
        contents = array[2]
    )
}

