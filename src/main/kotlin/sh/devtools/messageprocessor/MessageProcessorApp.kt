package sh.devtools.messageprocessor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MessageProcessorApp

fun main(args: Array<String>) {
	runApplication<MessageProcessorApp>(*args)
}
